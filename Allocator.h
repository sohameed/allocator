#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <cstddef>
#include <mutex>
#include <map>

// Meta data for blocks
struct Info
{
    int nextBlockId;        // will be storing -1 in it too, hence signed
    unsigned int signature; // will be storing a signature if the block is free
};

/////////////////// You may change these parameters ///////////////////
// define constants as static consts
// Change the following constants to increase the size of individual block, and the blocks pool.

// Size of a block in bytes
static const std::size_t BLOCK_SIZE = 16; // MUST BE GREATER THAN OR EQUAL TO sizeof(int)
static_assert(BLOCK_SIZE >= sizeof(Info), "BLOCK_SIZE MUST be greater than or equal to sizeof(int)");

// Total number of blocks.  First block is reserved, so essentially NUM_BLOCK-1 worth of blocks
// will be available for allocation
static const std::size_t NUM_BLOCKS = 10;
static_assert(NUM_BLOCKS > 1, "NUM_BLOCKS MUST be more than 1");

// Allocation Threshold.  This is the max number of blocks one thread will be allowed to allocate.  After
// this, it will continue to get nullptr until it deallocates one
static const std::size_t ALLOCATION_THRESHOLD = NUM_BLOCKS/2;
static_assert(ALLOCATION_THRESHOLD < NUM_BLOCKS, "ALLOCATION_THRESHOLD MUST be less the NUM_BLOCKS");

/////////////////// End of parameters ///////////////////

// Since only free blocks will use the nextBlockId, there is no need
// to separate it from the space for the block, hence declare a union
union Block
{
    struct Info info;
    unsigned char block[BLOCK_SIZE];
};

// Singleton class Allocator
class Allocator
{
public:
    // Returns instance
    // This is the only way to get access to the instance of this class, hence singleton
    static Allocator& getInstance();

    // Allocates a block
    // Non-blocking
    // Thread-safe
    // Returns nullptr if it could not allocate the block either because it could not lock, or if there are no free blocks
    // Won't let a thread allocate more than the ALLOCATION_THRESHOLD (defined in Allocator.h) number of blocks
    // The returned block is not initialized
    Block *allocate();

    // Deallocates a block.
    // Takes an address, and returns true if it could deallocate successfully, else returns false
    // Thread-safe
    // Double (multiple) deallocations are protected, returns true
    // Invalid (misaligned) addresses are detected, returns false
    bool deallocate(const Block *address);

    // Prints the linked list of the blocks (for debugging only)
    void printBlocks(std::string msg = std::string());

    // No copies allowed
    Allocator(Allocator const&) = delete;
    void operator=(Allocator const&) = delete;

    virtual ~Allocator() {}

protected:
    // No one can instantiate (except may be the derived classes)
    Allocator();

    // Blocks
    // Even though it is declared as a non-static member, since the class is a singleton and the instance is
    // declared as static, this will NOT be on stack, it will be in global space but protected within the
    // singleton instance.
    Block m_blocks[NUM_BLOCKS];

private:
    bool isAllowedToAllocate(const std::thread::id& threadId);
    void decrementAllocationScore(const std::thread::id& threadId);

    // Lock for thread-safety
    std::mutex m_lock;

    // Maps threads to number of pending allocations
    std::map<std::thread::id, unsigned int> m_score;

    static const unsigned int SIGNATURE = 0xDEADBEEF;
};

#endif // ALLOCATOR_H