This is an implementation of a simple fixed-block allocator.
The entire pool of memory is divided into equal sized blocks.  A call to allocate will allocate one block of memory.
The allocator is implemented using a linked list, but not quite in the traditional sense.   Instead of storing the pointer to the next block, it stores the index of the next block.  But logically it is still a linked list.
The allocator is memory efficient in that it uses the free blocks to save the meta data, or at least most of the meta data.  The meta data consists of the index of the next free block and a signature that indicates if the block is free or not.

# Features

 1. **Fully functional**.  The code is fully functional and buildable.  I have provided a sample main() function, which invokes multiple threads and they allocate and deallocate.  So far all the testing indicate that it is working fine.
 2. **Singleton**.  Only one instance of the allocator exists in the processs.  All the threads use the same allocator.
 3. **Thread-safe**.  Multiple threads can allocate and deallocate without stepping over each other.
 4. **Allocates** one block at a time.
 5. **Allocations** are non-blocking.  If some other thread is in the middle of allocating a block, or if there are no free blocks available, the allocate immediately returns with nullptr.
 6. **Fast Constant-time allocations**.  The allocation algorithm used has constant-time O(1) complexity.
 7. **Fast Constant-time deallocations**.  The deallocation algorithm used has constant-time O(1) complexity.
 8. **Double-free** is safe.  The caller can call deallocate as many times it wants but it won't mess up the internal data.
 9. **Invalid address protection** is implemented.  If the caller calls in to deallocate with a pointer that is not aligned with the block boundary, it will be rejected.
 10. **Starvation** is prevented by limiting the max number of pending allocated blocks in a thread.  There is a threshold that you can adjust which will be used to make sure that none of the threads could have more blocks allocated than the threshold. 

# Parameters
Following are the parameters that can be tweaked in **Allocator.h** file:

 1. **Block Size** in bytes.  It is currently set to 16 to make the testing easier.  But it can be adjusted to any number.  Must be greater than or equal to the size of struct Info (compile time check asserted).
 2. **Total Number of Blocks**.  This is the size of the pool.  It is currently set to 10 to make the testing easier.  This number should be greater than 1, because the block 0 is reserved and can't be allocated.
 3. **Allocation Threshold**.  This is used for starvation prevention.  This is the max number of allocated blocks any threads can have.  This is currently set to half of the total number of blocks. 

# How it all works
The pool (m_blocks) is initialized in the constructor such that block 0 points to block 1 and block 1 points to block 2 .. and so on, until the last block which points to nothing (indicated by -1 index). 
For example, if there are 10 blocks, this is what the blocks would look like after constructor initializes it:

> 1 2 3 4 5 6 7 8 9 -1

The block 0 is reserved and represents the head of the linked list, and hence cannot be returned as an allocated block.  Notice how all the blocks are linked together starting from block 0.
If now a call to allocate comes in, the block at the front of the list is returned.  This is accomplished by looking at the block 0 (which is the head of the list), and detaching the block which is pointed to by it.  In this case it is Block 1.  In order to detach block 1,  the next block id of block 0 is saved in a local variable, and then next block id of block 1 is copied over to the next block id of block 0.  This way block 1 gets detached from the linked list.

> 2 0 3 4 5 6 7 8 9 -1

Here 0 indicates that the block is allocated.  

If now another allocate request comes in, block 2 will be returned because block 2 is at the head of the list, and the blocks will look like this:

> 3 0 0 4 5 6 7 8 9 -1

If now block 1 is deallocated, block 1 needs to be placed back at the head of the list.  This is accomplished by copying the next block id of block 0 to next block id of block 1 and writing block 1 to the next block id of block 0.  This is what it will look like:

> 1 3 0 4 5 6 7 8 9 -1

If all the blocks are allocated, the pool would look like this:

> -1 0 0 0 0 0 0 0 0 0

# Data Structures Used
Following data structures were used in this implementation:

 1. **Linked List** - To implement the list of free blocks.
 2. **Map** - To keep track of the number of allocations for every thread.

# Future Improvements
These are some possible improvements:

 1. Starvation normally comes into picture when there are different priorities involved, like priorities of the threads.  This way higher priority threads might tend to allocate all the blocks, and the lower priority threads might starve out for the memory.  In that case we could do clever things like adjusting the priority of the waiting threads at run-time, thereby finally allowing them to allocate memory.
 2. Another scheme that could be implemented for allocation is to make allocation not only non-blocking but an asynchronous call.  This way the caller thread could allocate (or show the intention to allocate) and provide a callback which will be called when the memory is available.  In this mechanism the allocator can deploy various schemes, such as throttling back a frequently allocating thread, in order to give everyone a fair chance.  But I didn't want to go this far in this implementation.
 3. Currently the way it tells that the block is free or not is by looking at a fixed signature which is the value 0xDEADBEEF at a particular location in the block.  This signature is used to prevent double free of already freed block.  However, there is a small chance that the block which is about to be freed has the data that contains 0xDEADBEEF in that particular location.  This will result in that block never being freed.  To avoid this, we could use some hashing algorithm, such as SHA256 and store the hash of the block index in that location.  This will make it almost impossible to have any possible conflict with the user data in the block.
