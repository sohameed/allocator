#include <cstddef>
#include <iostream>
#include <thread>
#include <assert.h>

#include "Allocator.h"

// Returns singleton instance
// This is the only way to get access to the instance of this class, hence singleton
Allocator& Allocator::getInstance()
{
    // Declaring the instance as a static object
    // It won't be on stack
    static Allocator allocator;

    return allocator;
}

// Constructor
// Initializes meta data
Allocator::Allocator()
{
    // Initialize all the blocks into a linked list of blocks
    //
    // Iterate through all the blocks except the last one
    for (std::size_t blockIndex = 0; blockIndex < NUM_BLOCKS - 1; blockIndex++)
    {
        // Initialize all the ids in the bocks to point to the next block offset
        m_blocks[blockIndex].info.nextBlockId = blockIndex + 1;
        m_blocks[blockIndex].info.signature = SIGNATURE;
    }

    // Initialize the last block's nextBlockId to -1 to indicate end of the linked list
    m_blocks[NUM_BLOCKS - 1].info.nextBlockId = -1;
}

// Returns true if a thread is allowed to allocate, else false
// A thread is allowed to allocate only if it has allocated less than the allocation threshold.
bool Allocator::isAllowedToAllocate(const std::thread::id& threadId)
{
    const auto& it = m_score.find(threadId);

    bool isInitialized = false;

    // If an entry does not exist
    if (it == m_score.end())
    {
        // First time access, initialize
        m_score[threadId] = 0;
        isInitialized = true;
    }

    // If we initialized or if the score is less than or equal to the threshold, return true
    return (isInitialized || (it->second <= ALLOCATION_THRESHOLD));
}

// Allocates a block
// Non-blocking
// Thread-safe
// Returns nullptr if it could not allocate the block either because it could not lock, or if there are no free blocks
// Won't let a thread allocate more than the ALLOCATION_THRESHOLD (defined in Allocator.h) number of blocks
// The returned block is not initialized
Block *Allocator::allocate()
{
    // Initliaze the return pointer
    Block* pReturn = nullptr;

    bool isLocked = m_lock.try_lock();

    std::thread::id threadId = std::this_thread::get_id();

    if (isLocked && isAllowedToAllocate(threadId))
    {
        // If we have free blocks to allocate
        // (indicated by reserved block 0 nextBlockId other than -1)
        if (m_blocks[0].info.nextBlockId != -1)
        {
            // Save the address of the block pointed to by the nextBlockId of reserved block
            // That's the next free block available to allocate
            pReturn = &m_blocks[m_blocks[0].info.nextBlockId];
            std::size_t index = m_blocks[0].info.nextBlockId;

            // Now detach the block from the linked list by taking its nextBlockId and
            // storing it in the reserved block's nextBlockId for nexxt call into allocate.
            m_blocks[0].info.nextBlockId = m_blocks[m_blocks[0].info.nextBlockId].info.nextBlockId;
            m_blocks[index].info.nextBlockId = 0;  // Helps in debugging
            m_blocks[index].info.signature = 0;  // Remove the signature since now it is not free anymore

            // Increment the allocation score for the thread
            m_score[threadId]++;
        }
        // else there are no free blocks, return nullptr
    }
    // else could not lock, or not allowed to allocate, returnn nullptr

    // if we were able to lock, unlock before returning
    if (isLocked)
        m_lock.unlock();

    // Return the pointer, or the null pointer if could not allocate
    return pReturn;
}

// Decrements allocation score of the given thread
void Allocator::decrementAllocationScore(const std::thread::id& threadId)
{
    const auto &it = m_score.find(threadId);

    // if it is already there, decrement the allocation score
    bool isFound = it != m_score.end();

    // We have a bug in the code if isFound is false, assert
    assert(isFound);

    // Now decrement and if 0, remove the entry from the m_score map
    if (--(it->second) == 0)
        m_score.erase(it);
}

// Deallocates a block
// Takes an address, and returns true if it could deallocate successfully, else returns false
// Thread-safe
// Double (multiple) deallocates are protected, returns true
// Invalid addresses are detected and return false
bool Allocator::deallocate(const Block *address)
{
    bool isDeallocated = true;

    // First check to make sure the address is valid
    // Check to make sure if it is aligned properly

    // Compute the byte offset of the address from the beginning of the buffer
    std::ptrdiff_t voidOffset = (unsigned char *)address - (unsigned char *)m_blocks;

    // if the offset is not aligned with BLOCK_SIZE, return false
    if (voidOffset % BLOCK_SIZE != 0)
    {
        isDeallocated = false;
    }
    else  // the address is good, proceed.
    {
        std::ptrdiff_t offset = address - m_blocks;
        std::thread::id threadId = std::this_thread::get_id();

        // Lock before accessing critical data
        m_lock.lock();

        // Check for double (multiple) free.  Is the block already freed?
        // Could potentially conain the signature as "data" when used by the caller, but we will ignore it for now.
        // Could use a hash (SHA256 or something) of the offset for the signature to make it almost impossible to be a co-incidence
        if (m_blocks[offset].info.signature != SIGNATURE)
        {
            // insert the freed block at the head of the list
            m_blocks[offset].info.nextBlockId = m_blocks[0].info.nextBlockId;
            m_blocks[offset].info.signature = SIGNATURE;
            m_blocks[0].info.nextBlockId = offset;

            // Decrement the allocation score
            decrementAllocationScore(threadId);
        } 
        // else this is double allocation, ignore

        m_lock.unlock();
    }

    return isDeallocated;
}

void Allocator::printBlocks(std::string msg)
{
    m_lock.lock();

    std::cout << std::this_thread::get_id() << ": " << msg << ": ";
    for (std::size_t index = 0; index < NUM_BLOCKS; index++)
    {
        std::cout << m_blocks[index].info.nextBlockId << " ";
    }

    std::cout << std::endl;

    m_lock.unlock();
}
