#include <iostream>
#include <thread>

#include "Allocator.h"

void thread()
{
    while (true)
    {
        std::cout << std::this_thread::get_id() << ": "
                  << "+++++++++++++++++"
                  << "Allocating." << std::endl;

        Allocator::getInstance().printBlocks("Before");
        Block *pBlock = Allocator::getInstance().allocate();

        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        if (pBlock != nullptr)
        {
            Allocator::getInstance().printBlocks("Allocated");
            if (!Allocator::getInstance().deallocate(pBlock))
            {
                std::cout << "Failed to deallocate." << std::endl;
            }
            else
            {
                Allocator::getInstance().printBlocks("Deallocated");
                std::cout << "-------------------" << std::endl;
            }
        }
        else
        {
            std::cout << std::this_thread::get_id() << ": "
                      << "Could not allocate." << std::endl;
        }
    }
}

int main()
{
    Allocator::getInstance().printBlocks();
    Block *pBlock1 = Allocator::getInstance().allocate();
    Allocator::getInstance().printBlocks();
    Block *pBlock2 = Allocator::getInstance().allocate();
    Allocator::getInstance().printBlocks();
    Block *pBlock3 = Allocator::getInstance().allocate();
    Allocator::getInstance().printBlocks();

    if (pBlock1 && !Allocator::getInstance().deallocate(pBlock1))
        return 1;
    Allocator::getInstance().printBlocks();

    // Double de-allocate
    Allocator::getInstance().deallocate(pBlock1);
    Allocator::getInstance().printBlocks();

    if (pBlock2 && !Allocator::getInstance().deallocate(pBlock2))
        return 1;
    Allocator::getInstance().printBlocks();

    if (pBlock3 && !Allocator::getInstance().deallocate(pBlock3))
        return 1;
    Allocator::getInstance().printBlocks();

    static const std::size_t NUM_THREADS = 10;
    std::thread *pThreads[NUM_THREADS] = {nullptr};

    // Now Create threads
    for (std::size_t threadId = 0; threadId < NUM_THREADS; threadId++)
        pThreads[threadId] = new std::thread(thread);

    // Wait for all of them to finish (they won't though)
    for (std::size_t threadId = 0; threadId < NUM_THREADS; threadId++)
        pThreads[threadId]->join();

    return 0;
}
